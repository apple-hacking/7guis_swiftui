//
//  ContentView.swift
//  two
//
//  Created by slbtty on 2022-06-11.
//

import SwiftUI

// Two kind of change:
// User input and Auto Convert

// Only detecting onChange will result infinity loop

enum ChangeType {
    case user, machine
}

struct ContentView: View {
    @State var celsiusString = ""
    @State var celsiusChangeSource = ChangeType.user
    @State var farenString = ""
    @State var farenChangeSource = ChangeType.user

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                TextField("", text: $celsiusString)
                    .onChange(of: celsiusString) {
                        if case .user = celsiusChangeSource, let c = Double($0) {
                            farenChangeSource = .machine
                            farenString = String(format: "%.1f", c2f(c))
                        }
                        celsiusChangeSource = .user
                    }.frame(width: 100)
                Text("Celsius")
            }

            HStack {
                TextField("", text: $farenString)
                    .onChange(of: farenString) {
                        if case .user = farenChangeSource, let f = Double($0) {
                            celsiusChangeSource = .machine
                            celsiusString = String(format: "%.1f", f2c(f))
                        }
                        farenChangeSource = .user
                    }.frame(width: 100)
                Text("Farenheit")
            }
        }
        .padding()
    }
    private func c2f(_ c: Double) -> Double { c * 9 / 5 + 32 }
    private func f2c(_ f: Double) -> Double { (f - 32) * 5 / 9 }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

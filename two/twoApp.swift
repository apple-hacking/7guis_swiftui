//
//  twoApp.swift
//  two
//
//  Created by slbtty on 2022-06-11.
//

import SwiftUI

@main
struct twoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

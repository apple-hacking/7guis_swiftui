//
//  oneApp.swift
//  one
//
//  Created by slbtty on 2022-06-11.
//

import SwiftUI

@main
struct oneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

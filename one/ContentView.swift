//
//  ContentView.swift
//  one
//
//  Created by slbtty on 2022-06-11.
//

import SwiftUI

struct ContentView: View {
    
    @State var counter = 0
    
    
    var body: some View {
        HStack {
            Text("\(counter)")
            Button("Count") {
                counter += 1
                debugPrint(counter)
            }
            Button("Reset") {
                counter = 0
                debugPrint(counter)
            }
        }
        .padding(10.0)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
